@extends('layouts.app')

@section('content')

@foreach($pos as $a)
<div class="container1">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">

               
                
                <img src="{{ Auth::user()->avatar }}" cclass="mr-5 mt-2 rounded-circle" 
             style="width:50px ; height: 50px;border-radius: 40%;">
             

             <b> {{ Auth::user()->name }} </b>
                
                </div>
            
         
                <div class="card-body">
               
                   <img src="{{$a->image}}" style="width :100%;  height=350;" >
                 
                </div>

                <div class="card-footer">
                
                
                <b>{{ Auth::user()->email }}</b>
                <p>{{$a->caption}}</p>
                
               
                </div>
                
                </div>
               <br>
            </div>
            
        </div>
        @endforeach
       
@endsection
