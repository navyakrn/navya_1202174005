<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Illuminate\SUpport\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Navya Kirana',
            'email' => 'navyakrn99'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'avatar.jpg',
        ]);
        DB::table('users')->insert([
            'name' => 'navyakrn',
            'email' => 'abcd123'.'@gmail.com',
            'password' => bcrypt('password'),
            'avatar' => 'sansa.jpg',
        ]);
    }
}
