<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Illuminate\SUpport\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 
     */
    public function run()
    {
        //
        DB::table('posts')->insert([
            'user_id' => 10,
            'caption' => 'Childhood memory',
            'image' => 'avatar.jpg',
        ]);

        DB::table('posts')->insert([
            'user_id' => 10,
            'caption' => 'Arctic Monkeys',
            'image' => 'arcticmonkeys.jpg',
        ]);
        
        DB::table('posts')->insert([
            'user_id' => 10,
            'caption' => 'Stranger Things',
            'image' => 'hawkins.jpg',
        ]);
    }
}