<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::get('/profile/edit/{id}', 'ProfileController@edit')->name('profile');
Route::put('/profile/update/{id}', 'ProfileController@update');
Route::get('/newpost','NewPostController@index');
Route::get('/home/detail/{id}', 'HomeController@detail');
Route::get('/newpost/upload', 'NewPostController@upload');
Route::post('/newpost/upload', 'NewPostController@upload');
Route::get('/home/comment', 'HomeController@comment');
Route::post('/home/like', 'HomeController@like');
