@extends('layouts.app')
@section('content')
<div class="container">
	<div class="container" style="padding: 0 180px 0 180px;">
		
		<form action="{{url('edit')}}/{{ Auth::user()->id }}" method="post" enctype="multipart/form-data">
			<h1>Edit Profile</h1>
			<div class="form-group">
				@csrf
				
				
				@foreach($users as $user)
				<label for="exampleInputEmail1">Title</label>
				<input type="text" name="title" class="form-control" value="{{$user->title}}"><br>
				<label for="exampleInputEmail1">Description</label>
				<input type="text" name="description" class="form-control" value="{{$user->description}}"><br>
				<label for="exampleInputEmail1">URL</label>
				<input type="text" name="url" class="form-control" value="{{$user->url}}"><br>
				<label for="exampleFormControlFile1">Profile image</label>
				<input type="file" name="avatar" value="{{$user->avatar}}" class="form-control-file" id="exampleFormControlFile1">
				@endforeach
			</div>
			
			<button type="submit" class="btn btn-primary">Add new post</button>
		</form>
	</div>
	
	
	
</div>
@endsection