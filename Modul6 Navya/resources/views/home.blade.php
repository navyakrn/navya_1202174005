@extends('layouts.app')

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name='viewport' content='width=device-width, initial-scale=1'>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

@section('content')

@foreach($pos as $a)
<div class="container1">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">

               
                
                <img src="{{ Auth::user()->avatar }}" class="mr-5 mt-2 rounded-circle" 
             style="width:50px ; height: 50px;border-radius: 40%;">
             <b> {{ Auth::user()->name }} </b>
                </div>
            
         
                <div class="card-body">
                   <img src="{{$a->image}}" style="width :100%;  height=350;" >
                </div>

                <div class="card-footer">
                <a href="#"><i class="fa fa-heart-o"></i></a>
                            <a href="#"><i class="fa fa-comment-o"></i></a><br>
                                
                        <b>{{ Auth::user()->email }}</b>
                        <p>{{$a->caption}}</p>
                
                
                    <div class="input-group mb-3">
                     <input type="text" class="form-control" placeholder="Comment" aria-label="Recipient's username" aria-describedby="button-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="button-addon2">Post</button>
                </div>
                </div>
                </div>
                
                </div>
               <br>
            </div>
            
        </div>
        @endforeach
       
@endsection
