<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posts;
class NewPostController extends Controller
{
    public function index(){
        return view('newpost');

}
public function upload(Request $request){
    $this->validate($request, [
        'caption' => 'required',
        'image' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
    ]);
    $image = $request->file('image');

    $nama_file = time()."_".$image->getClientOriginalName();

    // tempat folder upload
    $tujuan_upload = 'uploaded_file';
    $image->move($tujuan_upload,$nama_file);
    posts::create([
        'user_id' => $request->userID,
        'caption' => $request->caption,
        'image' => $nama_file,
    ]);

    return redirect('/home');

}
}

