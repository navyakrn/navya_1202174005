<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name'=>'Navya Kirana',
            'email'=>'navyakrn99@gmail.com',
            'password'=>bcrypt('123456789'),
            'avatar'=>'avatar.jpg',
        ]);
    }
}
