<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 
     */
    public function run()
    {
        //
        DB::table('posts')->insert([
            'user_id' => 1,
            'caption' => 'Childhood memory',
            'image' => 'avatar.jpg',
        ]);
        DB::table('posts')->insert([
            'user_id' => 1,
            'caption' => 'Arctic Monkeys',
            'image' => 'arcticmonkeys.jpg',
        ]);
        DB::table('posts')->insert([
            'user_id' => 1,
            'caption' => 'Sansa Stark',
            'image' => 'sansa.jpg',
        ]);
    }
}